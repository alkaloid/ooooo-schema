const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  date: { type: Date, default: () => new Date() },
  ref_id: { type: mongoose.Schema.Types.ObjectId },
  profile_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Profile' },
  type: { type: String, lowercase: true, trim: true }
})

module.exports = schema
