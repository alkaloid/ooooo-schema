const mongoose = require('mongoose')
const { v4: uuidv4 } = require('uuid')
const { getNewVersion } = require('./utils')

const schema = new mongoose.Schema({
  created: {
    user_id: Object,
    date: { type: Date, default: () => new Date() }
  },
  modified: {
    user_id: mongoose.Schema.Types.ObjectId,
    date: Date
  },
  ext: {
    icon: String,
    splash: String,
    marker: String
  },
  pos: {
    x: Number,
    y: Number
  },
  size: {
    x: Number,
    y: Number
  },
  mysql_id: Number,
  name: String,
  info: String,
  play: String,
  uuid: { type: String, default: () => { return uuidv4() } },
  onmap: Boolean,
  enabled: Boolean,
  random: Boolean,
  free: Boolean,
  new: Boolean,
  sort: Number,
  category: String,
  icon_v: { type: Number, default: 0 },
  splash_v: { type: Number, default: 0 },
  marker_v: { type: Number, default: 0 },
  icon: {
    path: String,
    filename: String,
  },
  splash: {
    path: String,
    filename: String,
  },
  marker: {
    path: String,
    filename: String,
  },
  plays: { type: Number, default: 0 },
  apiid: String,
  favorites: { type: Number, default: 0 }
})

schema.index({ name: 1 }, { unique: true })
schema.index({ onmap: 1 })
schema.index({ enabled: 1 })
schema.index({ random: 1 })
schema.index({ plays: 1 })

schema.pre('findOneAndUpdate', async function (next) {
  if (this._update.icon || this._update.splash || this._update.marker) {
    const island = await this.model.findOne(this.getQuery())
    if (this._update.icon && this._update.icon.ext) {
      const version = getNewVersion(island.icon.filename)
      this._update.icon = {
        path: 'island/icon/',
        filename: `${island.uuid}${version ? '_' + version : ''}.${this._update.icon.ext}`
      }
    }
    if (this._update.splash && this._update.splash.ext) {
      const version = getNewVersion(island.splash.filename)
      this._update.splash = {
        path: 'island/splash/',
        filename: `${island.uuid}${version ? '_' + version : ''}.${this._update.splash.ext}`
      }
    }
    if (this._update.marker && this._update.marker.ext) {
      const version = getNewVersion(island.marker.filename)
      this._update.marker = {
        path: 'island/marker/',
        filename: `${island.uuid}${version ? '_' + version : ''}.${this._update.marker.ext}`
      }
    }
  }
  next()
})

module.exports = schema
