const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  username: { type: String, lowercase: true, trim: true },
  provisory: { type: String, lowercase: true, trim: true },
  created: { type: Date, default: () => new Date() },
  social: {
    id: String,
    media: String,
    email: String
  },
  password: String,
  enabled: { type: Boolean, default: 0 },
  role: { type: String, default: 'registered' },
  roles: [String],
  sessions: [String],
  ip: {
    country: String,
    address: String
  },
  modified: {
    user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    date: Date
  },
  code: Number,
  apple: {
    id: String,
    email: String,
    name: Object
  }
})

module.exports = schema
