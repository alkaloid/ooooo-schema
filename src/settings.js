const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  settings: {
    app: {
      startscreen: Boolean,
      context: Boolean
    },
    interface: {
      theme: String,
      density: String,
      darkmode: Boolean,
      tooltip: Boolean,
      splash: Boolean
    },
    map: {
      background: String,
      colorized: Boolean
    },
    player: {
      mapoverlay: Boolean,
      autoplay: Boolean
    }
  }
})

module.exports = schema
