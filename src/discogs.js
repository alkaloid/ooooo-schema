const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  date: { type: Date, default: () => new Date() },
  track_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Track' },
  results: [Object]
})

schema.index({ track_id: 1 }, { unique: true })

module.exports = schema
