const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  date: { type: Date, default: () => new Date() },
  follower: { type: mongoose.Schema.Types.ObjectId, ref: 'Profile' },
  following: { type: mongoose.Schema.Types.ObjectId, ref: 'Profile' }
})

schema.index({ follower: 1, following: 1 }, { unique: true })
schema.index({ date: 1 })

module.exports = schema
