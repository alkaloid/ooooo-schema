const mongoose = require('mongoose')
const { v4: uuidv4 } = require('uuid')

const schema = new mongoose.Schema({
  mysql_id: Number,
  created: {
    user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    date: { type: Date, default: () => new Date() }
  },
  modified: {
    user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    date: Date
  },
  validated: {
    user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    date: Date
  },
  profile_id: { type: mongoose.Schema.Types.ObjectId, ref: 'profile' },
  status: { type: String, lowercase: true, trim: true, default: 'posted' },
  title: { type: String, trim: true },
  artist: { type: String, trim: true },
  album: { type: String, trim: true },
  songwriter: { type: String, trim: true },
  producer: { type: String, trim: true },
  label: { type: String, trim: true },
  country: { type: String, default: 'UND' },
  info: String,
  genre: String,
  mood: { type: String, default: 'SLOW' },
  year: String,
  decade: { type: Number, default: 3000 },
  length: Number,
  likes: { type: Number, default: 0 },
  bitrate: Number,
  color: String,
  uuid: { type: String, default: () => { return uuidv4() } },
  ext: {
    cover: { type: String, default: 'jpg' },
    track: String
  },
  links: {
    itunes: Object,
    discogg: Object
  },
  ISR: String,
  islands: [{ type: mongoose.Schema.Types.ObjectId, ref: 'island' }],
  image: { type: Boolean, default: false },
  image_v: { type: Number, default: 0 },
  isIslandOnly: { type: Boolean, default: false },
  duplicate_id: { type: mongoose.Schema.Types.ObjectId, ref: 'track' },
  plays: { type: Number, default: 0 },
  artwork: { type: String, trim: true },
  cover: {
    path: String,
    filename: String,
    color: String
  }
})

schema.pre('save', async function (next) {
  if (!this.mood) { this.mood = 'SLOW' }
  this.decade = this.year - this.year % 10
  if (this[Symbol.for('image')]) {
    this.cover.path = `cover/${this.country}/${this.decade}/`
    this.cover.filename = `${this.uuid}${this.image_v ? '_' + this.image_v : ''}.${this.ext.cover}`
  }
  next()
})

module.exports = schema
