const mongoose = require('mongoose')
const { v4: uuidv4 } = require('uuid')

const schema = new mongoose.Schema({
  created: {
    user_id: mongoose.Schema.Types.ObjectId,
    date: { type: Date, default: () => new Date() }
  },
  modified: {
    user_id: mongoose.Schema.Types.ObjectId,
    date: Date
  },
  published: {
    user_id: mongoose.Schema.Types.ObjectId,
    date: Date
  },
  uuid: { type: String, default: () => uuidv4() },
  category_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Category' },
  name: String,
  title: {
    type: String,
    unique: true
  },
  summary: String,
  content: String,
  status: String,
  mysql_id: Number
})

module.exports = schema
