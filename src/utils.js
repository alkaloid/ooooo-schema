module.exports = {
  getNewVersion: filename => {
    if (!filename) return 1
    if (!filename.match(/_(.*?)\./)) return 2

    return +filename.match(/_(.*?)\./)[1] + 1
  }
}
