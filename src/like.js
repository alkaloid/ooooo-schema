const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  mysql_id: Number,
  date: { type: Date, default: () => new Date() },
  track_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Track' },
  profile_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Profile' }
})

schema.index({ track_id: 1, profile_id: 1 }, { unique: true })
schema.index({ date: 1 })

module.exports = schema
