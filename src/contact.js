const mongoose = require('mongoose')
const ObjectId = mongoose.Schema.Types.ObjectId

const schema = new mongoose.Schema({
  user_id: { type: ObjectId, ref: 'User' },
  name: String,
  title: String,
  firstname: String,
  lastname: String,
  address: String,
  address2: String,
  place: String,
  zipcode: String,
  country: String,
  email: { type: String, lowercase: true, trim: true },
  phone: {
    mobile: String,
    other: String
  },
  modified: {
    user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    date: Date
  }
})

module.exports = schema
