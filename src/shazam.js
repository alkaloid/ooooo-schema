const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  date: { type: Date, default: () => new Date() },
  track_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Track' },
  title: String,
  isrc: String,
  key: String,
  artist: String,
  url: String,
  image: {
    artist: String,
    cover: String
  },
  album: String,
  label: String,
  year: String
})

schema.index({ track_id: 1 }, { unique: true })

module.exports = schema
