const mongoose = require('mongoose')
const ObjectId = mongoose.Schema.Types.ObjectId

const schema = new mongoose.Schema({
  user_id: { type: ObjectId, ref: 'User' },
  customer_id: String,
  pm_id: String,
  last4digit: String,
  exp_date: String,
  brand: String,
  plan: String,
  currency: String,
  amount: String,
  status: String,
  country: String,
  date: { type: Date, default: () => new Date() },
  type: String,
  trans_id: String,
  expiry_date: Date,
  receipt: String
})

module.exports = schema
