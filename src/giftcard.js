const mongoose = require('mongoose')
const subscription = require('./subscription')
const { v4: uuidv4 } = require('uuid')

const ObjectId = mongoose.Schema.Types.ObjectId

const schema = new mongoose.Schema({
  subscription: { type: ObjectId, ref: 'subscription' },
  code: String,
  email: String,
  user_id: { type: ObjectId, ref: 'user' },
  date: {
    type: Date,
    default: () => new Date()
  },
  plan: String,
  redeemed: { type: Boolean, default: false },
  redeemed_date: Date,
  uuid: { type: String, default: () => uuidv4() },
  count: { type: Number, default: 1 },
})

schema.pre('updateOne', async function (next) {
  const giftcard = await this.model.findOne(this.getQuery())
  if (this._update.$inc && this._update.$inc.count && this._update.$inc.count === -1 && giftcard.count === 1) {
    this._update.$set = { ...this._update.$set, redeemed: true }
  }
  next()
})

module.exports = schema
