const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  profile_id: { type: mongoose.Schema.Types.ObjectId, ref: 'profile' },
  device_id: String
})

schema.index({ device_id: 1 }, { unique: true })
schema.index({ profile_id: 1 })

module.exports = schema
