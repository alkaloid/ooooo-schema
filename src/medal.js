const mongoose = require('mongoose')
const { v4: uuidv4 } = require('uuid')

const schema = new mongoose.Schema({
  mysql_id: Number,
  name: String,
  type: String,
  score: Number,
  uuid: { type: String, default: () => { return uuidv4() } },
  created: {
    user_id: mongoose.Types.ObjectId,
    date: { type: Date, default: () => new Date() }
  },
  modified: {
    user_id: mongoose.Types.ObjectId,
    date: Date
  }
})

schema.index({ name: 1 }, { unique: true })

module.exports = schema
