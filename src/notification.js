const mongoose = require('mongoose')

const ref = {
  id: mongoose.Schema.Types.ObjectId,
  image: {
    path: String,
    filename: String,
    color: String
  },
  name: String,
  title: String,
  count: Number
}

const schema = new mongoose.Schema({
  sender: {
    id: { type: mongoose.Schema.Types.ObjectId, ref: 'Profile' },
    pseudonym: String
  },
  reciever: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Profile' }],
  type: String,
  message: String,
  date: { type: Date, default: () => new Date() },
  ref: ref
})

// notification seen automatically expire after one month

schema.index({ date: 1 }, { expireAfterSeconds: 60 * 60 * 24 * 30 })

schema.index({ reciever: 1, 'sender.id': 1, type: 1, 'ref.id': 1 }, { unique: true })
schema.index({ 'sender.id': 1 })
schema.index({ reciever: 1 })

module.exports = schema
