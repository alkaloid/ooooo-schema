const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  date: { type: Date, default: () => new Date() },
  profile_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Profile' },
  track_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Track' },
  context: {
    moods: [String],
    decades: [Number],
    isocodes: [String],
    island: { type: mongoose.Schema.Types.ObjectId, ref: 'Island' },
    mode: String
  },
  count: { type: Number, default: 0 },
  type: { type: String, default: () => 'track' }
})

module.exports = schema
