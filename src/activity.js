const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  date: { type: Date, default: () => new Date() },
  user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  name: String,
  ref: mongoose.Schema.Types.ObjectId,
  error: String,
  ip: String,
  type: { type: String, lowercase: true }
})

// schema.index({ date: 1 }, { expireAfterSeconds: 60 * 60 * 24 * 30 })

schema.index({ user_id: 1 })

schema.index({ ref: 1 })

schema.index({ name: 1 })

module.exports = schema
