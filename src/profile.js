const mongoose = require('mongoose')
const { v4: uuidv4 } = require('uuid')

const ObjectId = mongoose.Schema.Types.ObjectId

// key v : Version of the image on s3 server

const schema = new mongoose.Schema({
  user_id: { type: ObjectId, ref: 'User' },
  pseudonym: String,
  info: String,
  country: String,
  birthday: Date,
  uuid: { type: String, default: () => { return uuidv4() } },
  medals: [{ type: ObjectId, ref: 'Medal' }],
  ranking: { type: Number, default: 0 },
  created: { type: Date, default: () => new Date() },
  lastSeen: { type: Date, default: () => new Date() },
  image_v: Number,
  mysql_id: Number,
  likes: Number,
  trackRejected: Boolean,
  modified: {
    user_id: { type: ObjectId, ref: 'user' },
    date: Date
  }
})

module.exports = schema
