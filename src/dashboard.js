const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  name: { type: String },
  type: { type: String, lowercase: true },
  data: { 
  	type: String
  }
})

module.exports = schema
