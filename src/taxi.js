const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    context: Object,
    profile_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Profile' },
    date: { type: Date, default: () => new Date() },
    name: { type: String, required: false, maxLength: 64 }
})

schema.index({ profile_id: 1 })
schema.index({ date: 1 })
schema.index({ profile_id: 1, name: 1 }, { unique: true })

module.exports = schema
