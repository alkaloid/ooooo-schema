const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  session: String,
  track_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Track' },
  country: String,
  date: { type: Date, default: () => new Date(), expires: 60 * 60 * 24 * 30 * 6 }
})

module.exports = schema
