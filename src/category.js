const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  name: String,
  type: String,
  order: Number
})

module.exports = schema
